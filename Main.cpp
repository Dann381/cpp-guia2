#include <iostream>
using namespace std;
#include "Pila.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

int main(){
    Clear();
    int tamano;
    cout << "Ingrese tamaño máximo de la pila: ";
    cin >> tamano;
    Pila pila = Pila(tamano);

    int salir = 0;
    int opt;
    Clear();
    while (salir != 1){
        cout << "Tamaño de la pila: " << tamano << endl;
        cout << "[1] Agregar/push" << endl;
        cout << "[2] Remover/pop" << endl;
        cout << "[3] Ver pila" << endl;
        cout << "[4] Salir" << endl;
        cout << "Ingrese una opción: ";
        cin >> opt;
        if (opt == 1){
            Clear();
            cout << "Ingrese el número que desea agregar: ";
            int dato;
            cin >> dato;
            Clear();
            pila.Push(dato);
        }
        else if (opt == 2){
            Clear();
            pila.Pop();
        }
        else if (opt == 3){
            Clear();
            cout << "Mostrando datos guardados en la pila:\n" << endl;
            pila.Ver_pila();
        }
        else if (opt == 4){
            Clear();
            cout << "Saliendo\n" << endl;
            salir = 1;
        }
        else {
            Clear();
            cout << "Ingrese un número válido\n" << endl;
        }
    }

    return 0;
}