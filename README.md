# Guía 2

Este repositorio contiene los archivos necesarios para poder ejecutar lo solicitado en la guía número 2 del curso de Algoritmo y Estructura de Datos.

### Descripción
Este ejercicio crea un arreglo llamado "datos", el cual se comporta como un arreglo de tipo "pila" que recibe números enteros. Dentro de los archivos se encuentra el Makefile, lo que permite compilar los archivos, entregando un ejecutable llamado "programa". Al ejecutarlo, se le pedirá al usuario que ingrese el tamaño de la pila, para luego mostrarse un menú que le permite al usuario agregar números, borrar números, ver los elementos guardados y salir del programa. 

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
