#include <iostream>
using namespace std;
#include "Pila.h"

Pila::Pila(){
    int max;
    int tope = -1;
    int *datos = NULL;
    bool band;

}

Pila::Pila(int max){
    this->max = max;
    this->datos = new int[this->max];
}

void Pila::Pila_vacia(){
    if (this->tope == -1){
        this->band = true; //La pila está vacía
    }
    else {
        this->band = false; //La pila no está vacía
    }
}

void Pila::Pila_llena(){
    if (this->tope == this->max-1){
        this->band = true; //La pila está llena
    }
    else {
        this->band = false; //La pila no está llena
    }
}

void Pila::Push(int dato){
    this->Pila_llena();
    if (this->band == true){
        cout << "Desbordamiento, pila llena\n" << endl;
    }
    else {
        this->tope++;
        this->datos[this->tope] = dato;
        cout << "Dato agregado correctamente\n" << endl;
    }
}

void Pila::Pop(){
    this->Pila_vacia();
    if (this->band == true){
        cout << "Subdesbordamiento, pila vacía\n" << endl;
    }
    else {
        int borrado = this->datos[this->tope];
        this->tope = this->tope - 1;
        cout << "Se eliminó el dato: " << borrado << "\n"<< endl;
    }
}

void Pila::Ver_pila(){
    int inicio = this->tope;
    for (int i = inicio; i > -1; i--){
        cout << this->datos[i] << endl;
    }
    cout << endl;   
}

