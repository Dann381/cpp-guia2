#ifndef PILA_H
#define PILA_H

class Pila{
    private:
        int max;
        int tope = -1;
        int *datos = NULL;
        bool band;
    public:
        Pila();
        Pila(int max);

        void Pila_vacia();
        void Pila_llena();
        void Push(int dato);
        void Pop();
        void Ver_pila();
};
#endif